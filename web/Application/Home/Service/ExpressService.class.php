<?php

namespace Home\Service;

use Home\DAO\ExpressDAO;

/**
 * 运单 Service
 *
 * @author 李静波
 */
class ExpressService extends PSIBaseService {
	private $LOG_CATEGORY = "运单管理";

	/**
	 * 寄件
	 */
	public function addExpressBill($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->addExpressBill($params);
	}

	/**
	 * 运单列表
	 */
	public function expressList($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->expressList($params);
	}

	/**
	 * 运单变更详情
	 */
	public function expressTracing($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->expressTracing($params);
	}

	/**
	 * 变更运单
	 */
	public function editExpressBill($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->editExpressBill($params);
	}

	/**
	 * 获得某个运单的详情
	 */
	public function expressBillInfo($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->expressBillInfo($params);
	}

	public function companyList() {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->companyList();
	}

	public function editCompany($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->editCompany($params);
	}

	public function deleteCompany($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->deleteCompany($params);
	}

	public function getExpressBillTemplateInfo($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->getExpressBillTemplateInfo($params);
	}

	public function editExpressBillTemplate($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->editExpressBillTemplate($params);
	}

	public function getTemplateInfo($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->getTemplateInfo($params);
	}

	public function getTemplateInfoForDesign($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->getTemplateInfoForDesign($params);
	}

	public function designExpressBillTemplate($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->designExpressBillTemplate($params);
	}

	public function deleteDesignBill($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->deleteDesignBill($params);
	}

	public function editSenderInfo($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->editSenderInfo($params);
	}

	public function getSenderInfo() {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$ed = new ExpressDAO();
		
		return $ed->getSenderInfo();
	}
}