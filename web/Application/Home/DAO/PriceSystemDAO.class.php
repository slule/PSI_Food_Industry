<?php

namespace Home\DAO;

use Home\Service\IdGenService;
use Home\Service\UserService;

/**
 * 价格体系 DAO
 *
 * @author 李静波
 */
class PriceSystemDAO extends PSIBaseDAO {
	private $LOG_CATEGORY = "价格体系";

	/**
	 * 价格列表
	 */
	public function priceSystemList() {
		$db = M();
		$sql = "select id, name, factor from t_price_system";
		return $db->query($sql);
	}

	/**
	 * 新增或编辑价格体系中的价格
	 */
	public function editPriceSystem($params) {
		$id = $params["id"];
		$name = $params["name"];
		$factor = $params["factor"];
		
		$factor = floatval($factor);
		if ($factor < 0) {
			return $this->bad("基准价格倍数不能是负数");
		}
		
		$db = M();
		
		$db->startTrans();
		
		$us = new UserService();
		$companyId = $us->getCompanyId();
		$dataOrg = $us->getLoginUserDataOrg();
		
		$log = null;
		if ($id) {
			// 编辑
			// 检查价格是否已经存在
			$sql = "select count(*) as cnt from t_price_system 
					where name = '%s' and id <> '%s' ";
			$data = $db->query($sql, $name, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("价格[$name]已经存在");
			}
			
			$sql = "update t_price_system
					set name = '%s', factor = %f
					where id = '%s' ";
			
			$rc = $db->execute($sql, $name, $factor, $id);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "编辑价格体系-价格[$name]";
		} else {
			// 新增
			// 检查价格是否已经存在
			$sql = "select count(*) as cnt from t_price_system where name = '%s' ";
			$data = $db->query($sql, $name);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("价格[$name]已经存在");
			}
			
			$idGen = new IdGenService();
			
			$id = $idGen->newId($db);
			
			$sql = "insert into t_price_system(id, name, data_org, company_id, factor)
					values ('%s', '%s', '%s', '%s', %f)";
			$rc = $db->execute($sql, $id, $name, $dataOrg, $companyId, $factor);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "新增价格体系-价格[$name]";
		}
		
		if ($log) {
			$blDAO = new BizlogDAO($db);
			$blDAO->insertBizlog($log, $this->LOG_CATEGORY);
		}
		
		$db->commit();
		
		return $this->ok($id);
	}

	/**
	 * 删除价格
	 */
	public function deletePriceSystem($params) {
		$id = $params["id"];
		
		$db = M();
		
		$db->startTrans();
		
		// 检查要删除的价格是否存在
		$sql = "select name from t_price_system where id = '%s' ";
		$data = $db->query($sql, $id);
		if (! $data) {
			$db->rollback();
			return $this->bad("要删除的价格不存在");
		}
		$name = $data[0]["name"];
		
		// 检查该价格是否已经被使用
		$sql = "select count(*) as cnt from t_customer_category
				where ps_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			$db->rollback();
			return $this->bad("价格[$name]在客户分类中使用了，不能删除");
		}
		
		$sql = "select count(*) as cnt from t_customer
				where ps_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			$db->rollback();
			return $this->bad("价格[$name]在客户中使用了，不能删除");
		}
		
		$sql = "delete from t_price_system where id = '%s' ";
		$rc = $db->execute($sql, $id);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$log = "删除价格体系-价格[$name]";
		$blDAO = new BizlogDAO($db);
		$blDAO->insertBizlog($log, $this->LOG_CATEGORY);
		
		$db->commit();
		
		return $this->ok();
	}

	/**
	 * 查询某个商品的所有价格体系里面的价格列表
	 */
	public function goodsPriceSystemList($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "select p.name, g.price
				from t_price_system p
				left join  t_goods_price g
				on p.id = g.ps_id
				and g.goods_id = '%s' ";
		$data = $db->query($sql, $id);
		
		$result = array();
		
		foreach ( $data as $i => $v ) {
			$result[$i]["name"] = $v["name"];
			$result[$i]["price"] = $v["price"];
		}
		
		return $result;
	}

	/**
	 * 查询某个商品的价格体系中所有价格的值
	 */
	public function goodsPriceSystemInfo($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "select p.id, p.name, p.factor, g.price
				from t_price_system p
				left join  t_goods_price g
				on p.id = g.ps_id
				and g.goods_id = '%s' ";
		$data = $db->query($sql, $id);
		
		$result = array();
		
		foreach ( $data as $i => $v ) {
			$result[$i]["id"] = $v["id"];
			$result[$i]["name"] = $v["name"];
			$result[$i]["factor"] = $v["factor"];
			$result[$i]["price"] = $v["price"];
		}
		
		$sql = "select base_sale_price from t_goods
				where id = '%s' ";
		$data = $db->query($sql, $id);
		$baseSalePrice = $data[0]["base_sale_price"];
		
		return array(
				"priceList" => $result,
				"baseSalePrice" => $baseSalePrice
		);
	}

	/**
	 * 设置商品价格体系中的价格
	 */
	public function editGoodsPriceSystem($params) {
		$json = $params["jsonStr"];
		$bill = json_decode(html_entity_decode($json), true);
		if ($bill == null) {
			return $this->bad("传入的参数错误，不是正确的JSON格式");
		}
		
		$db = M();
		
		$db->startTrans();
		
		$goodsId = $bill["id"];
		$baseSalePrice = $bill["basePrice"];
		
		$sql = "select code, name, spec from t_goods
				where id = '%s' ";
		$data = $db->query($sql, $goodsId);
		if (! $data) {
			$db->rollback();
			return $this->bad("商品不存在");
		}
		$code = $data[0]["code"];
		$name = $data[0]["name"];
		$spec = $data[0]["spec"];
		
		$sql = "update t_goods
				set base_sale_price = %f
				where id = '%s' ";
		$rc = $db->execute($sql, $baseSalePrice, $goodsId);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$sql = "delete from t_goods_price where goods_id = '%s' ";
		$rc = $db->execute($sql, $goodsId);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$items = $bill["items"];
		
		$idGen = new IdGenService();
		
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		
		foreach ( $items as $v ) {
			$psId = $v["id"];
			$price = $v["price"];
			
			$id = $idGen->newId($db);
			
			$sql = "insert into t_goods_price (id, goods_id, ps_id, price, data_org, company_id)
					values ('%s', '%s', '%s', %f, '%s', '%s')";
			$rc = $db->execute($sql, $id, $goodsId, $psId, $price, $dataOrg, $companyId);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
		}
		
		$log = "设置商品[$code $name $spec]的价格体系";
		$blDAO = new BizlogDAO($db);
		$blDAO->insertBizlog($log, $this->LOG_CATEGORY);
		
		$db->commit();
		
		return $this->ok();
	}
}