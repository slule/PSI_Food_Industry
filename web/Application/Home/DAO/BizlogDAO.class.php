<?php

namespace Home\DAO;

use Home\Service\UserService;

/**
 * 业务日志 DAO
 *
 * @author 李静波
 */
class BizlogDAO extends PSIBaseDAO {
	var $db;

	function __construct($db) {
		$this->db = $db;
	}

	/**
	 * 记录业务日志
	 *
	 * @param string $log
	 *        	日志内容
	 * @param string $category
	 *        	日志分类
	 */
	public function insertBizlog($log, $category = "系统") {
		$us = new UserService();
		if ($us->getLoginUserId() == null) {
			return;
		}
		
		$ip = session("PSI_login_user_ip");
		if ($ip == null || $ip == "") {
			$ip = $this->getClientIP();
		}
		
		$ipFrom = session("PSI_login_user_ip_from");
		
		$db = $this->db;
		
		$companyId = $us->getCompanyId();
		
		$sql = "insert into t_biz_log (user_id, info, ip, date_created, log_category, data_org,
							ip_from, company_id)
						values ('%s', '%s', '%s',  now(), '%s', '%s', '%s', '%s')";
		$db->execute($sql, $us->getLoginUserId(), $log, $ip, $category, $dataOrg, $ipFrom, 
				$companyId);
	}

	private function getClientIP() {
		return get_client_ip();
	}
}