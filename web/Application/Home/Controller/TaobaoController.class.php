<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\UserService;
use Think\Controller;
use Home\Service\TaobaoService;

/**
 * 淘宝店铺Controller
 *
 * @author 李静波
 *        
 */
class TaobaoController extends PSIBaseController {

	/**
	 * 淘宝店宝贝管理 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::TAOBAO_GOODS)) {
			$this->initVar();
			
			$this->assign("title", "淘宝店宝贝管理");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Taobao/index");
		}
	}

	/**
	 * 淘宝店铺管理 - 主页面
	 */
	public function shopIndex() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::TAOBAO_SHOP)) {
			$this->initVar();
			
			$this->assign("title", "淘宝店铺管理");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Taobao/shopIndex");
		}
	}

	/**
	 * 淘宝店铺列表
	 */
	public function shopList() {
		if (IS_POST) {
			$ts = new TaobaoService();
			
			$this->ajaxReturn($ts->shopList());
		}
	}

	/**
	 * 新增或编辑淘宝店铺
	 */
	public function editShop() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"name" => I("post.name")
			);
			
			$ts = new TaobaoService();
			$this->ajaxReturn($ts->editShop($params));
		}
	}

	/**
	 * 删除淘宝店铺
	 */
	public function deleteShop() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ts = new TaobaoService();
			
			$this->ajaxReturn($ts->deleteShop($params));
		}
	}
}