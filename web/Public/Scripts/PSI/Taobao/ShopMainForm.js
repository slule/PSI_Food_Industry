/**
 * 淘宝店铺管理 - 主界面
 * 
 * @author 李静波
 */
Ext.define("PSI.Taobao.ShopMainForm", {
			extend : "Ext.panel.Panel",
			border : 0,
			layout : "border",

			config : {
				pAdd : null,
				pEdit : null,
				pDelete : null,
				pEditDataOrg : null
			},

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				Ext.apply(me, {
							tbar : [{
										text : "新增淘宝店铺",
										iconCls : "PSI-button-add",
										handler : me.onAddShop,
										scope : me
									}, {
										text : "编辑淘宝店铺",
										iconCls : "PSI-button-edit",
										handler : me.onEditShop,
										scope : me
									}, {
										text : "删除淘宝店铺",
										iconCls : "PSI-button-delete",
										handler : me.onDeleteShop,
										scope : me
									}, "-", {
										text : "关闭",
										iconCls : "PSI-button-exit",
										handler : function() {
											location
													.replace(PSI.Const.BASE_URL);
										}
									}],
							items : [{
										region : "center",
										xtype : "panel",
										layout : "fit",
										border : 0,
										items : [me.getMainGrid()]
									}]
						});

				me.callParent(arguments);

				me.freshGrid();
			},

			/**
			 * 新增淘宝店铺
			 */
			onAddShop : function() {
				var me = this;
				var form = Ext.create("PSI.Taobao.ShopEditForm", {
							parentForm : me
						});

				form.show();
			},

			/**
			 * 编辑淘宝店铺
			 */
			onEditShop : function() {
				var me = this;

				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要编辑的淘宝店铺");
					return;
				}

				var shop = item[0];

				var form = Ext.create("PSI.Taobao.ShopEditForm", {
							parentForm : me,
							entity : shop
						});

				form.show();
			},

			/**
			 * 删除淘宝店铺
			 */
			onDeleteShop : function() {
				var me = this;
				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要删除的淘宝店铺");
					return;
				}

				var shop = item[0];
				var info = "请确认是否删除淘宝店铺 <span style='color:red'>"
						+ shop.get("name") + "</span> ?";

				var store = me.getMainGrid().getStore();
				var index = store.findExact("id", shop.get("id"));
				index--;
				var preIndex = null;
				var preShop = store.getAt(index);
				if (preShop) {
					preIndex = preShop.get("id");
				}

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL + "Home/Taobao/deleteShop",
						params : {
							id : shop.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshGrid(preIndex);
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				};

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			freshGrid : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL + "Home/Taobao/shopList",
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);

									me.gotoGridRecord(id);
								}

								el.unmask();
							}
						});
			},

			gotoGridRecord : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var store = grid.getStore();
				if (id) {
					var r = store.findExact("id", id);
					if (r != -1) {
						grid.getSelectionModel().select(r);
					} else {
						grid.getSelectionModel().select(0);
					}
				}
			},

			getMainGrid : function() {
				var me = this;
				if (me.__mainGrid) {
					return me.__mainGrid;
				}

				var modelName = "PSITaobaoShop";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name"]
						});

				me.__mainGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "淘宝店铺名称",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										width : 400
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {
								itemdblclick : {
									fn : me.onEditShop,
									scope : me
								}
							}
						});

				return me.__mainGrid;
			}
		});