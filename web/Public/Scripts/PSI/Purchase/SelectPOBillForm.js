/**
 * 采购入库单 - 选择采购订单界面
 */
Ext.define("PSI.Purchase.SelectPOBillForm", {
	extend : "Ext.window.Window",
	config : {
		parentForm : null
	},
	initComponent : function() {
		var me = this;
		Ext.apply(me, {
					title : "选择采购订单",
					modal : true,
					onEsc : Ext.emptyFn,
					width : 1000,
					height : 600,
					layout : "border",
					items : [{
								region : "center",
								border : 0,
								bodyPadding : 10,
								layout : "fit",
								items : [me.getPOBillGrid()]
							}, {
								region : "north",
								border : 0,
								layout : {
									type : "table",
									columns : 2
								},
								height : 170,
								bodyPadding : 10,
								items : [{
											html : "<h1>选择采购订单</h1>",
											border : 0,
											colspan : 2
										}, {
											id : "editSelectQueryRef",
											labelWidth : 100,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "单号",
											margin : "5, 0, 0, 0",
											xtype : "textfield"
										}, {
											id : "editSelectQueryFromDT",
											xtype : "datefield",
											margin : "5, 0, 0, 0",
											format : "Y-m-d",
											labelAlign : "right",
											labelSeparator : "",
											labelWidth : 100,
											fieldLabel : "交货日期（起）"
										}, {
											id : "editSelectQueryToDT",
											xtype : "datefield",
											margin : "5, 0, 0, 0",
											format : "Y-m-d",
											labelAlign : "right",
											labelSeparator : "",
											labelWidth : 100,
											fieldLabel : "交货日期（止）"
										}, {
											id : "editSelectQuerySupplier",
											xtype : "psi_supplierfield",
											parentCmp : me,
											labelAlign : "right",
											labelSeparator : "",
											labelWidth : 60,
											margin : "5, 0, 0, 0",
											labelWidth : 100,
											fieldLabel : "供应商"
										}, {
											xtype : "container",
											items : [{
														xtype : "button",
														text : "查询",
														width : 100,
														margin : "10 0 0 10",
														iconCls : "PSI-button-refresh",
														handler : me.onQuery,
														scope : me
													}, {
														xtype : "button",
														text : "清空查询条件",
														width : 100,
														margin : "10, 0, 0, 10",
														handler : me.onClearQuery,
														scope : me
													}]
										}]
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						}
					},
					buttons : [{
								text : "选择",
								iconCls : "PSI-button-ok",
								formBind : true,
								handler : me.onOK,
								scope : me
							}, {
								text : "取消",
								handler : function() {
									me.close();
								},
								scope : me
							}]
				});

		me.callParent(arguments);

		me.onQuery();
	},

	onWndShow : function() {
		var me = this;
	},

	onOK : function() {
		var me = this;

		var item = me.getPOBillGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择采购订单");
			return;
		}
		var bill = item[0];
		me.close();
		me.getParentForm().getPOBillInfo(bill.get("id"));
	},

	getPOBillGrid : function() {
		var me = this;

		if (me.__billGrid) {
			return me.__billGrid;
		}

		var modelName = "PSIPWBill_POSelectForm";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "ref", "supplierName", "contact", "tel",
							"fax", "inputUserName", "bizUserName",
							"goodsMoney", "dateCreated", "paymentType", "tax",
							"moneyWithTax", "dealDate", "dealAddress",
							"orgName", "confirmUserName", "confirmDate",
							"billMemo", "freight", "billStatus"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : [],
					pageSize : 20,
					proxy : {
						type : "ajax",
						actionMethods : {
							read : "POST"
						},
						url : PSI.Const.BASE_URL
								+ "Home/Purchase/selectPOBillList",
						reader : {
							root : 'dataList',
							totalProperty : 'totalCount'
						}
					}
				});

		store.on("beforeload", function() {
					store.proxy.extraParams = me.getQueryParam();
				});

		me.__billGrid = Ext.create("Ext.grid.Panel", {
					columnLines : true,
					columns : [Ext.create("Ext.grid.RowNumberer", {
										text : "序号",
										width : 50
									}), {
								header : "状态",
								dataIndex : "billStatus",
								menuDisabled : true,
								sortable : false,
								width : 80,
								renderer : function(value) {
									if (value == 0) {
										return "<span style='color:red'>待审核</span>";
									} else if (value == 1000) {
										return "已审核";
									} else if (value == 2000) {
										return "部分入库";
									} else if (value == 3000) {
										return "全部入库";
									} else if (value > 3000) {
										return "订单关闭";
									} else {
										return "";
									}
								}
							}, {
								header : "采购订单号",
								dataIndex : "ref",
								width : 110,
								menuDisabled : true,
								sortable : false
							}, {
								header : "交货日期",
								dataIndex : "dealDate",
								menuDisabled : true,
								sortable : false
							}, {
								header : "交货地址",
								dataIndex : "dealAddress",
								menuDisabled : true,
								sortable : false
							}, {
								header : "供应商",
								dataIndex : "supplierName",
								width : 300,
								menuDisabled : true,
								sortable : false
							}, {
								header : "供应商联系人",
								dataIndex : "contact",
								menuDisabled : true,
								sortable : false
							}, {
								header : "供应商电话",
								dataIndex : "tel",
								menuDisabled : true,
								sortable : false
							}, {
								header : "供应商传真",
								dataIndex : "fax",
								menuDisabled : true,
								sortable : false
							}, {
								header : "采购金额",
								dataIndex : "goodsMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								width : 150
							}, {
								header : "税金",
								dataIndex : "tax",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								width : 150
							}, {
								header : "价税合计",
								dataIndex : "moneyWithTax",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								width : 150
							}, {
								header : "付款方式",
								dataIndex : "paymentType",
								menuDisabled : true,
								sortable : false,
								width : 100,
								renderer : function(value) {
									if (value == 0) {
										return "记应付账款";
									} else if (value == 1) {
										return "现金付款";
									} else if (value == 2) {
										return "预付款";
									} else {
										return "";
									}
								}
							}, {
								header : "运费",
								dataIndex : "freight",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "业务员",
								dataIndex : "bizUserName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "组织机构",
								dataIndex : "orgName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "制单人",
								dataIndex : "inputUserName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "制单时间",
								dataIndex : "dateCreated",
								menuDisabled : true,
								sortable : false,
								width : 140
							}, {
								header : "审核人",
								dataIndex : "confirmUserName",
								menuDisabled : true,
								sortable : false
							}, {
								header : "审核时间",
								dataIndex : "confirmDate",
								menuDisabled : true,
								sortable : false,
								width : 140
							}, {
								header : "备注",
								dataIndex : "billMemo",
								menuDisabled : true,
								sortable : false
							}],
					listeners : {
						itemdblclick : {
							fn : me.onOK,
							scope : me
						}
					},
					store : store
				});

		return me.__billGrid;
	},

	onQuery : function() {
		var me = this;
		me.getPOBillGrid().getStore().load();
	},

	getQueryParam : function() {
		var me = this;

		var result = {};

		var ref = Ext.getCmp("editSelectQueryRef").getValue();
		if (ref) {
			result.ref = ref;
		}

		var supplierId = Ext.getCmp("editSelectQuerySupplier").getIdValue();
		if (supplierId) {
			result.supplierId = supplierId;
		}

		var fromDT = Ext.getCmp("editSelectQueryFromDT").getValue();
		if (fromDT) {
			result.fromDT = Ext.Date.format(fromDT, "Y-m-d");
		}

		var toDT = Ext.getCmp("editSelectQueryToDT").getValue();
		if (toDT) {
			result.toDT = Ext.Date.format(toDT, "Y-m-d");
		}

		return result;
	},

	onClearQuery : function() {
		var me = this;

		Ext.getCmp("editSelectQueryRef").setValue(null);
		Ext.getCmp("editSelectQueryFromDT").setValue(null);
		Ext.getCmp("editSelectQueryToDT").setValue(null);
		Ext.getCmp("editSelectQuerySupplier").clearIdValue();

		me.onQuery();
	}
});