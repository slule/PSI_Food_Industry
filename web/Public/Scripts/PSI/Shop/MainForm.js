/**
 * 店铺管理 - 主界面
 */
Ext.define("PSI.Shop.MainForm", {
			extend : "Ext.panel.Panel",
			border : 0,
			layout : "border",

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				Ext.apply(me, {
							tbar : [{
										text : "新增店铺",
										iconCls : "PSI-button-add",
										handler : me.onAddShop,
										scope : me
									}, {
										text : "编辑店铺",
										iconCls : "PSI-button-edit",
										handler : me.onEditShop,
										scope : me
									}, {
										text : "删除店铺",
										iconCls : "PSI-button-delete",
										handler : me.onDeleteShop,
										scope : me
									}, "-", {
										text : "设置店员",
										iconCls : "PSI-button-edit-detail",
										handler : me.onEditShopUser,
										scope : me
									}, "-", {
										text : "关闭",
										iconCls : "PSI-button-exit",
										handler : function() {
											location
													.replace(PSI.Const.BASE_URL);
										}
									}],
							items : [{
										region : "center",
										xtype : "panel",
										layout : "fit",
										border : 0,
										items : [me.getMainGrid()]
									}, {
										region : "south",
										layout : "fit",
										height : "40%",
										split : true,
										items : [me.getUserGrid()]
									}]
						});

				me.callParent(arguments);

				me.freshGrid();
			},

			/**
			 * 新增
			 */
			onAddShop : function() {
				var form = Ext.create("PSI.Shop.EditForm", {
							parentForm : this
						});

				form.show();
			},

			/**
			 * 编辑
			 */
			onEditShop : function() {
				var me = this;

				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要编辑的店铺");
					return;
				}

				var shop = item[0];

				var form = Ext.create("PSI.Shop.EditForm", {
							parentForm : me,
							entity : shop
						});

				form.show();
			},

			/**
			 * 删除
			 */
			onDeleteShop : function() {
				var me = this;
				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要删除的店铺");
					return;
				}

				var shop = item[0];
				var info = "请确认是否删除店铺 <span style='color:red'>"
						+ shop.get("name") + "</span> ?";

				var store = me.getMainGrid().getStore();
				var index = store.findExact("id", shop.get("id"));
				index--;
				var preIndex = null;
				var pre = store.getAt(index);
				if (pre) {
					preIndex = pre.get("id");
				}

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL + "Home/Shop/deleteShop",
						params : {
							id : shop.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshGrid(preIndex);
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				};

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			freshGrid : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL + "Home/Shop/shopList",
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);

									me.gotoGridRecord(id);
								}

								el.unmask();
							}
						});
			},

			gotoGridRecord : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var store = grid.getStore();
				if (id) {
					var r = store.findExact("id", id);
					if (r != -1) {
						grid.getSelectionModel().select(r);
					} else {
						grid.getSelectionModel().select(0);
					}
				}
			},

			getMainGrid : function() {
				var me = this;
				if (me.__mainGrid) {
					return me.__mainGrid;
				}

				var modelName = "PSIShop";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "code", "name", "address",
									"masterName", "shopType", "usePOS",
									"warehouseName"]
						});

				me.__mainGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "店铺编码",
										dataIndex : "code",
										menuDisabled : true,
										sortable : false,
										width : 60
									}, {
										header : "店铺名称",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										width : 200
									}, {
										header : "店铺地址",
										dataIndex : "address",
										menuDisabled : true,
										sortable : false,
										width : 200
									}, {
										header : "店长",
										dataIndex : "masterName",
										menuDisabled : true,
										sortable : false
									}, {
										header : "店铺类型",
										dataIndex : "shopType",
										menuDisabled : true,
										sortable : false
									}, {
										header : "收银系统",
										dataIndex : "usePOS",
										menuDisabled : true,
										sortable : false
									}, {
										header : "发货出库仓库",
										dataIndex : "warehouseName",
										menuDisabled : true,
										sortable : false,
										width : 150
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {
								itemdblclick : {
									fn : me.onEditShop,
									scope : me
								},
								select : {
									fn : me.onShopSelect,
									scope : me
								}
							}
						});

				return me.__mainGrid;
			},

			getUserGrid : function() {
				var me = this;
				if (me.__userGrid) {
					return me.__userGrid;
				}

				var modelName = "PSIShopUser";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "loginName", "orgName", "code", "name"]
						});

				me.__userGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							title : "店员",
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "用户编码",
										dataIndex : "code",
										menuDisabled : true,
										sortable : false
									}, {
										header : "用户登录名",
										dataIndex : "loginName",
										menuDisabled : true,
										sortable : false,
										width : 200
									}, {
										header : "姓名",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										width : 200
									}, {
										header : "组织机构",
										dataIndex : "orgName",
										menuDisabled : true,
										sortable : false,
										width : 500
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {}
						});

				return me.__userGrid;
			},

			/**
			 * 设置店员
			 */
			onEditShopUser : function() {
				var me = this;

				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择店铺");
					return;
				}

				var shop = item[0];

				var form = Ext.create("PSI.Shop.ShopUserEditForm", {
							entity : shop,
							parentForm : me
						});

				form.show();
			},

			onShopSelect : function() {
				var me = this;
				me.freshUserGrid();
			},
			
			freshUserGrid: function() {
				var me = this;

				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					return;
				}

				var shop = item[0];

				var grid = me.getUserGrid();
				var el = grid.getEl();
				el.mask(PSI.Const.LOADING);

				var r = {
					url : PSI.Const.BASE_URL + "Home/Shop/userList",
					params : {
						id : shop.get("id")
					},
					method : "POST",
					callback : function(options, success, response) {
						var store = grid.getStore();

						store.removeAll();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							store.add(data);
						}

						el.unmask();
					}
				};
				Ext.Ajax.request(r);
			}
		});