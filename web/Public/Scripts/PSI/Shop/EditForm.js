/**
 * 店铺 - 新增或编辑界面
 */
Ext.define("PSI.Shop.EditForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		entity : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		var entity = me.getEntity();

		me.adding = entity == null;

		var buttons = [];
		if (!entity) {
			var btn = {
				text : "保存并继续新增",
				formBind : true,
				handler : function() {
					me.onOK(true);
				},
				scope : me
			};

			buttons.push(btn);
		}

		var btn = {
			text : "保存",
			formBind : true,
			iconCls : "PSI-button-ok",
			handler : function() {
				me.onOK(false);
			},
			scope : me
		};
		buttons.push(btn);

		var btn = {
			text : entity == null ? "关闭" : "取消",
			handler : function() {
				me.close();
			},
			scope : me
		};
		buttons.push(btn);

		Ext.apply(me, {
					title : entity == null ? "新增店铺" : "编辑店铺",
					modal : true,
					resizable : false,
					onEsc : Ext.emptyFn,
					width : 600,
					height : 210,
					layout : "fit",
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						},
						close : {
							fn : me.onWndClose,
							scope : me
						}
					},
					items : [{
						id : "editForm",
						xtype : "form",
						layout : {
							type : "table",
							columns : 2
						},
						height : "100%",
						bodyPadding : 5,
						defaultType : 'textfield',
						fieldDefaults : {
							labelWidth : 60,
							labelAlign : "right",
							labelSeparator : "",
							msgTarget : 'side',
							margin : "5",
							width : 270
						},
						items : [{
									xtype : "hidden",
									name : "id",
									value : entity == null ? null : entity
											.get("id")
								}, {
									id : "editCode",
									fieldLabel : "店铺编码",
									allowBlank : false,
									blankText : "没有输入店铺编码",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									name : "code",
									value : entity == null ? null : entity
											.get("code"),
									listeners : {
										specialkey : {
											fn : me.onEditSpecialKey,
											scope : me
										}
									}
								}, {
									id : "editName",
									fieldLabel : "店铺名称",
									allowBlank : false,
									blankText : "没有输入店铺名称",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									name : "name",
									value : entity == null ? null : entity
											.get("name"),
									listeners : {
										specialkey : {
											fn : me.onEditSpecialKey,
											scope : me
										}
									}
								}, {
									id : "editAddress",
									fieldLabel : "店铺地址",
									name : "address",
									value : entity == null ? null : entity
											.get("address"),
									colspan : 2,
									width : 550,
									listeners : {
										specialkey : {
											fn : me.onEditSpecialKey,
											scope : me
										}
									}
								}, {
									id : "editMaster",
									xtype : "psi_userfield",
									fieldLabel : "店长",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									listeners : {
										specialkey : {
											fn : me.onEditSpecialKey,
											scope : me
										}
									}
								}, {
									id : "editMasterId",
									xtype : "hidden",
									name : "masterId"
								}, {
									id : "editShopType",
									fieldLabel : "店铺类型",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									xtype : "combo",
									queryMode : "local",
									editable : false,
									valueField : "id",
									labelSeparator : "",
									store : Ext.create("Ext.data.ArrayStore", {
												fields : ["id", "text"],
												data : [["0", "实体店"],
														["1", "淘宝店"],
														["-1", "其他"]]
											}),
									value : "0",
									name : "shopType",
									listeners : {
										specialkey : {
											fn : me.onEditSpecialKey,
											scope : me
										}
									}

								}, {
									xtype : 'fieldcontainer',
									fieldLabel : '收银系统',
									defaultType : 'radiofield',
									defaults : {
										flex : 1
									},
									layout : 'hbox',
									items : [{
												boxLabel : '启用',
												name : 'usePOS',
												inputValue : '1',
												id : 'radioUsePOS',
												checked : true
											}, {
												boxLabel : '停用',
												name : 'usePOS',
												inputValue : '0',
												id : 'radioStopPOS',
												checked : false
											}]
								}, {
									id : "editWarehouse",
									xtype : "psi_warehousefield",
									beforeLabelTextTpl : PSI.Const.REQUIRED,
									fieldLabel : "出库仓库",
									listeners : {
										specialkey : {
											fn : me.onLastEditSpecialKey,
											scope : me
										}
									}

								}, {
									id : "editWarehouseId",
									xtype : "hidden",
									name : "warehouseId"
								}],
						buttons : buttons
					}]
				});

		me.callParent(arguments);

		me.__editorList = ["editCode", "editName", "editAddress", "editMaster",
				"editShopType", "editWarehouse"];
	},

	/**
	 * 保存
	 */
	onOK : function(thenAdd) {
		var me = this;

		var masterId = Ext.getCmp("editMaster").getIdValue();
		if (!masterId) {
			PSI.MsgBox.showInfo("没有输入店长");
			return;
		}
		Ext.getCmp("editMasterId").setValue(masterId);

		var warehouseId = Ext.getCmp("editWarehouse").getIdValue();
		if (!warehouseId) {
			PSI.MsgBox.showInfo("没有输入出库仓库");
			return;
		}
		Ext.getCmp("editWarehouseId").setValue(warehouseId);

		var f = Ext.getCmp("editForm");
		var el = f.getEl();
		el.mask(PSI.Const.SAVING);
		var sf = {
			url : PSI.Const.BASE_URL + "Home/Shop/editShop",
			method : "POST",
			success : function(form, action) {
				me.__lastId = action.result.id;

				el.unmask();

				PSI.MsgBox.tip("数据保存成功");
				me.focus();
				if (thenAdd) {
					me.clearEdit();
				} else {
					me.close();
				}
			},
			failure : function(form, action) {
				el.unmask();
				PSI.MsgBox.showInfo(action.result.msg, function() {
							Ext.getCmp("editCode").focus();
						});
			}
		};
		f.submit(sf);
	},

	onEditSpecialKey : function(field, e) {
		if (e.getKey() === e.ENTER) {
			var me = this;
			var id = field.getId();
			for (var i = 0; i < me.__editorList.length; i++) {
				var editorId = me.__editorList[i];
				if (id === editorId) {
					var edit = Ext.getCmp(me.__editorList[i + 1]);
					edit.focus();
					edit.setValue(edit.getValue());
				}
			}
		}
	},

	onLastEditSpecialKey : function(field, e) {
		var me = this;

		if (e.getKey() == e.ENTER) {
			var f = Ext.getCmp("editForm");
			if (f.getForm().isValid()) {
				me.onOK(me.adding);
			}
		}
	},

	clearEdit : function() {
		Ext.getCmp("editCode").focus();

		var editors = [Ext.getCmp("editCode"), Ext.getCmp("editName"),
				Ext.getCmp("editAddress")];
		for (var i = 0; i < editors.length; i++) {
			var edit = editors[i];
			edit.setValue(null);
			edit.clearInvalid();
		}
	},

	onWndClose : function() {
		var me = this;
		if (me.__lastId) {
			me.getParentForm().freshGrid(me.__lastId);
		}
	},

	onWndShow : function() {
		var me = this;

		var editCode = Ext.getCmp("editCode");
		editCode.focus();
		editCode.setValue(editCode.getValue());

		if (me.getEntity() == null) {
			return;
		}

		var el = me.getEl();
		el.mask(PSI.Const.LOADING);
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL + "Home/Shop/shopInfo",
					params : {
						id : me.getEntity().get("id")
					},
					method : "POST",
					callback : function(options, success, response) {
						el.unmask();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							if (data.code) {
								Ext.getCmp("editCode").setValue(data.code);
								Ext.getCmp("editName").setValue(data.name);
								Ext.getCmp("editAddress")
										.setValue(data.address);
								Ext.getCmp("editShopType")
										.setValue(data.shopType);
								Ext.getCmp("radioUsePOS").setValue(data.usePOS);
								Ext.getCmp("radioStopPOS")
										.setValue(!data.usePOS);

								if (data.masterId) {
									var edit = Ext.getCmp("editMaster");
									edit.setValue(data.masterName);
									edit.setIdValue(data.masterId);
								}
								if (data.warehouseId) {
									var edit = Ext.getCmp("editWarehouse");
									edit.setValue(data.warehouseName);
									edit.setIdValue(data.warehouseId);
								}
							}
						}
					}
				});
	}
});