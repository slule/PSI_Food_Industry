/**
 * 人员职务 - 主界面
 */
Ext.define("PSI.UserPosition.MainForm", {
			extend : "Ext.panel.Panel",
			border : 0,
			layout : "border",

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				Ext.apply(me, {
							tbar : [{
										text : "新增职务",
										iconCls : "PSI-button-add",
										handler : me.onAddPosition,
										scope : me
									}, {
										text : "编辑职务",
										iconCls : "PSI-button-edit",
										handler : me.onEditPosition,
										scope : me
									}, {
										text : "删除职务",
										iconCls : "PSI-button-delete",
										handler : me.onDeletePosition,
										scope : me
									}, "-", {
										text : "关闭",
										iconCls : "PSI-button-exit",
										handler : function() {
											location
													.replace(PSI.Const.BASE_URL);
										}
									}],
							items : [{
										region : "center",
										xtype : "panel",
										layout : "fit",
										border : 0,
										items : [me.getMainGrid()]
									}]
						});

				me.callParent(arguments);

				me.freshGrid();
			},

			/**
			 * 新增人员职务
			 */
			onAddPosition : function() {
				var me = this;

				var form = Ext.create("PSI.UserPosition.EditForm", {
							parentForm : me
						});
				form.show();
			},

			/**
			 * 编辑人员职务
			 */
			onEditPosition : function() {
				var me = this;

				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要编辑的职务");
					return;
				}

				var position = item[0];

				var form = Ext.create("PSI.UserPosition.EditForm", {
							parentForm : me,
							entity : position
						});
				form.show();
			},

			/**
			 * 删除人员职务
			 */
			onDeletePosition : function() {
				var me = this;
				var item = me.getMainGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要删除的职务");
					return;
				}

				var position = item[0];
				var info = "请确认是否删除职务 <span style='color:red'>"
						+ position.get("name") + "</span> ?";

				var store = me.getMainGrid().getStore();
				var index = store.findExact("id", position.get("id"));
				index--;
				var preIndex = null;
				var prePosition = store.getAt(index);
				if (prePosition) {
					preIndex = prePosition.get("id");
				}

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL
								+ "Home/User/deleteUserPosition",
						params : {
							id : position.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshGrid(preIndex);
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				};

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			getMainGrid : function() {
				var me = this;
				if (me.__mainGrid) {
					return me.__mainGrid;
				}

				var modelName = "PSIUserPosition";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name", "companyName"]
						});

				me.__mainGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "职务",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										width : 300
									}, {
										header : "所属公司",
										dataIndex : "companyName",
										menuDisabled : true,
										sortable : false,
										width : 200
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {
								itemdblclick : {
									fn : me.onEditWarehouse,
									scope : me
								}
							}
						});

				return me.__mainGrid;
			},

			freshGrid : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL
									+ "Home/User/userPositionList",
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);

									me.gotoGridRecord(id);
								}

								el.unmask();
							}
						});
			},

			gotoGridRecord : function(id) {
				var me = this;
				var grid = me.getMainGrid();
				var store = grid.getStore();
				if (id) {
					var r = store.findExact("id", id);
					if (r != -1) {
						grid.getSelectionModel().select(r);
					} else {
						grid.getSelectionModel().select(0);
					}
				}
			}
		});