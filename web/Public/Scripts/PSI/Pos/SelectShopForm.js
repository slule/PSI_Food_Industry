/**
 * 选择店铺
 */
Ext.define("PSI.Pos.SelectShopForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		shopData : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		var buttons = [];

		var btn = {
			text : "选择",
			formBind : true,
			iconCls : "PSI-button-ok",
			handler : function() {
				me.onOK();
			},
			scope : me
		};
		buttons.push(btn);

		Ext.apply(me, {
					title : "选择店铺",
					modal : true,
					resizable : false,
					closable : false,
					onEsc : Ext.emptyFn,
					width : 400,
					height : 110,
					layout : "fit",
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						},
						close : {
							fn : me.onWndClose,
							scope : me
						}
					},
					items : [{
								id : "editForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 60,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 370,
									margin : "5"
								},
								items : [{
											id : "editShop",
											xtype : "combo",
											queryMode : "local",
											editable : false,
											valueField : "id",
											displayField : "name",
											labelWidth : 60,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "店铺",
											margin : "5, 0, 0, 0",
											store : Ext.create(
													"Ext.data.ArrayStore", {
														fields : ["id", "name"],
														data : []
													}),
											listeners : {
												specialkey : {
													fn : me.onEditShopSpecialKey,
													scope : me
												}
											}
										}],
								buttons : buttons
							}]
				});

		me.callParent(arguments);
	},

	onOK : function() {
		var me = this;

		var editShop = Ext.getCmp("editShop");
		var shopId = editShop.getValue();
		if (shopId == null) {
			PSI.MsgBox.showInfo("没有选择店铺", function() {
						editShop.focus();
					});
			return;
		}

		var store = editShop.getStore();
		var index = store.findExact("id", shopId);
		if (index > -1) {
			var shop = store.getAt(index);
			me.getParentForm().setCurrentShopInfo({
						id : shop.get("id"),
						name : shop.get("name")
					});
			me.close();
		} else {
			PSI.MsgBox.showInfo("没有选择店铺", function() {
						editShop.focus();
					});
		}
	},

	onWndShow : function() {
		var me = this;

		var editShop = Ext.getCmp("editShop");
		var store = editShop.getStore();
		store.removeAll();
		var data = me.getShopData();
		store.add(data);

		if (data.length > 0) {
			editShop.setValue(data[0]["id"]);
		}

		editShop.focus();
	},

	onEditShopSpecialKey : function(field, e) {
		var me = this;
		if (e.getKey() == e.ENTER) {
			// me.onOK();
		}
	}
});