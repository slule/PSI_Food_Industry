/**
 * 选择商品
 */
Ext.define("PSI.Pos.SelectGoodsForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		goodsData : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		var buttons = [];

		var btn = {
			text : "选择",
			formBind : true,
			iconCls : "PSI-button-ok",
			handler : function() {
				me.onOK();
			},
			scope : me
		};
		buttons.push(btn);

		Ext.apply(me, {
					title : "选择商品",
					modal : true,
					resizable : false,
					// onEsc : Ext.emptyFn,
					width : 860,
					height : 500,
					layout : "border",
					items : [{
								region : "center",
								layout : "fit",
								border : 0,
								bodyPadding : 10,
								items : [me.getGoodsGrid()]
							}, {
								region : "south",
								height : 40,
								border : 0,
								bodyPadding : 10,
								items : [{
											id : "editCodeSelect",
											width : 830,
											xtype : "textfield",
											listeners : {
												specialkey : {
													fn : me.onEditCodeSpecialKey,
													scope : me
												}
											}
										}]
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						},
						close : {
							fn : me.onWndClose,
							scope : me
						}
					},
					buttons : buttons
				});

		me.callParent(arguments);
	},

	onOK : function() {
		var me = this;

		var grid = me.getGoodsGrid();
		var item = grid.getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}

		var goods = item[0].getData();

		var data = [];
		data.push(goods);
		me.getParentForm().setGoodsInfo("", data);
		me.close();
	},

	onWndShow : function() {
		var me = this;
		var store = me.getGoodsGrid().getStore();
		store.removeAll();
		store.add(me.getGoodsData());
		me.getGoodsGrid().getSelectionModel().select(0);

		Ext.getCmp("editCodeSelect").focus();
	},

	onWndClose : function() {
		var me = this;
		var form = me.getParentForm();
		form.focus();
		form.afterSelectGoodsFormClosed();
	},

	getGoodsGrid : function() {
		var me = this;
		if (me.__goodsGrid) {
			return me.__goodsGrid;
		}

		var modelName = "PSIPosGoods_SelectGoodsForm";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["goodsId", "goodsCode", "goodsName", "goodsSpec",
							"unitName", "goodsPrice"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : []
				});

		me.__goodsGrid = Ext.create("Ext.grid.Panel", {
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [Ext.create("Ext.grid.RowNumberer", {
										text : "序号",
										width : 40
									}), {
								header : "商品编码",
								dataIndex : "goodsCode",
								menuDisabled : true,
								sortable : false,
								width : 120
							}, {
								header : "商品名称",
								dataIndex : "goodsName",
								menuDisabled : true,
								sortable : false,
								width : 200
							}, {
								header : "规格型号",
								dataIndex : "goodsSpec",
								menuDisabled : true,
								sortable : false,
								width : 200
							}, {
								header : "单位",
								dataIndex : "unitName",
								menuDisabled : true,
								sortable : false,
								width : 60
							}, {
								header : "销售单价",
								dataIndex : "goodsPrice",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								width : 150
							}],
					store : store
				});

		return me.__goodsGrid;
	},

	onEditCodeSpecialKey : function(field, e) {
		var me = this;
		var editName = Ext.getCmp("editCodeSelect");

		if (e.getKey() == e.ENTER) {
			me.onOK();
		} else if (e.getKey() == e.UP) {
			var m = me.getGoodsGrid().getSelectionModel();
			var store = me.getGoodsGrid().getStore();
			var index = 0;
			for (var i = 0; i < store.getCount(); i++) {
				if (m.isSelected(i)) {
					index = i;
				}
			}
			index--;
			if (index < 0) {
				index = 0;
			}
			m.select(index);
			e.preventDefault();
			editName.focus();
		} else if (e.getKey() == e.DOWN) {
			var m = me.getGoodsGrid().getSelectionModel();
			var store = me.getGoodsGrid().getStore();
			var index = 0;
			for (var i = 0; i < store.getCount(); i++) {
				if (m.isSelected(i)) {
					index = i;
				}
			}
			index++;
			if (index > store.getCount() - 1) {
				index = store.getCount() - 1;
			}
			m.select(index);
			e.preventDefault();
			editName.focus();
		}
	}
});